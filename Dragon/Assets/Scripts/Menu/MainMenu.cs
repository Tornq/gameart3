using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace PTWO_PR
{
    public class MainMenu : MonoBehaviour
    {
        //Credits panel
        [SerializeField] GameObject credits;
        //Bool to check if the game has started
        bool hasStarted;

        //references to buttons
        [SerializeField] GameObject startButton, continueButton;

        public AudioSource audioSource;

        //Audio that should play in the game
        [SerializeField] private AudioClip gameAudio;

        private void Start()
        {
            //pause the game
            Time.timeScale = 0f;
            audioSource.volume = PlayerPrefs.GetFloat("MusicVol");
        }
       
        private void Update()
        {
            //If the game hasn't started 
            if(hasStarted == false)
            {
                //We have a Start Button
                startButton.SetActive(true);
                continueButton.SetActive(false);
            }
            //If the game has started 
            else
            {
                //We have a continue button
                startButton.SetActive(false);
                continueButton.SetActive(true);
            }
        }

        //Starts the game and plays the right audio
        public void StartGame()
        {
            audioSource.clip = gameAudio;
            audioSource.Play();
            hasStarted = true;
            Time.timeScale = 1f;
            gameObject.SetActive(false);
        }

        //Continues the game and plays the right audio
        public void ContinueGame()
        {
            audioSource.clip = gameAudio;
            audioSource.Play();
            hasStarted = true;
            Time.timeScale = 1f;
            gameObject.SetActive(false);
        }

        //Opens the credits ppanel
        public void OpenCredits()
        {
            credits.SetActive(true);
        }

        //Closes the credits panel
        public void CloseCredits()
        {
            credits.SetActive(false);
        }

        //Quits the game 
        public void QuitGame()
        {
            Application.Quit();
        }
    }
}

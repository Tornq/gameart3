﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using FSM;

namespace PTWO_PR
{
    public class PauseMenu : MonoBehaviour
    {
        //reference to pause menu
        public GameObject pauseMenu;

        //reference to main menu
        [SerializeField] private GameObject mainMenu;

        public AudioSource audioSource;

        //Audio that should play in the main menu
        [SerializeField] private AudioClip mainMenuAudio;

        //Checking if menu is active 
        private bool isMenuActive = false;

        //Reference to the slider
        public Slider slider; 

        //FSM
        private StateMachine fsm;
        //number to set the state of the fsm
        [SerializeField] private int number;
        //Bool if we are changing the slider
        public bool setLevel; 

        private void Start()
        {
            //Set the number to 0/RESTING
            number = 0; 

            fsm = new StateMachine();
            //Adding all states
            fsm.AddState("RESTING",  OnEnterResting);
            fsm.AddState("INUSE", onLogic : OnLogicInUse);
            //Adding all transitions
            fsm.AddTransition("RESTING", "INUSE", FromRestingToInUse);
            fsm.AddTransition("INUSE", "RESTING", FromInUseToResting);
            //Set start state
            fsm.SetStartState("RESTING");
            fsm.Init(); 
        }

        private void Update()
        {
            fsm.OnLogic(); 

            //If we hit esc and the menu is not active, we activate it. Otherwise we hide it
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (isMenuActive)
                {
                    Resume();
                }
                else
                {
                    Pause();
                }
            }

        }

        //Pauses the game and activates the panel
        public void Pause()
        {
            pauseMenu.SetActive(true);
            isMenuActive = true;
            Time.timeScale = 0f;
        }

        //Resmues the game and deactivates the panel
        public void Resume()
        {
            pauseMenu.SetActive(false);
            isMenuActive = false;
            Time.timeScale = 1f;

        }

        //When we change the slider value, we set the number to 1/INUSE
        public void BeginDrag()
        {
            number = 1;  
        }

        //When we stop changing the slider value, we set the number to 0/RESTING
        public void EndDrag()
        {
            number = 0; 
        }

        //Sets the audio volume to the slider value
        public void ChangeValue()
        {

            audioSource.volume = slider.value;
        }

        //Returns back to main menu 
        public void LoadMenu()
        {
            audioSource.clip = mainMenuAudio;
            audioSource.Play();
            mainMenu.SetActive(true);
            pauseMenu.SetActive(false);
        }

        //Quits the game
        public void Quit()
        {
            Application.Quit();
        }

        //State that gets called once when we enter resting/number = 0
        void OnEnterResting(State<string> obj)
        {
            //Updating the slider to volume
            slider.value = audioSource.volume;

            Debug.Log("On enter Resting"); 
        }
        
        //State that gets called every frame when we are changing the value of the slider
        void OnLogicInUse(State<string> obj)
        {
            //Saving the value
            PlayerPrefs.SetFloat("MusicVol", slider.value);

            Debug.Log("On logic in use");
        }

        //Transition into the inuse state when we are changing the value of the slider
        bool FromRestingToInUse(Transition<string> arg)
        {
            if (number == 1)
            {
                Debug.Log("Transition to InUse"); 
            }

            return number ==1 ; 
        }

        //Transition into the resting state when we are no longer changing the value of the slider
        bool FromInUseToResting(Transition<string> arg)
        {
            if (number ==0)
            {
                Debug.Log("Transition to Resting");
            }

            return number == 0; 
        }
    }
}





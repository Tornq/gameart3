using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PTWO_PR
{
    public class CinemachineSwitcher : MonoBehaviour
    {
        //References 
        [SerializeField] private GameObject heal, meele, range, tank, dragon;
        [SerializeField] private Animator camAnim;

        //Checking is unit camera animation was set
        public bool wasSet;

        // Update is called once per frame
        void Update()
        {
            //If it's the healer's turn and the animation wasn't set yet 
            if(heal.activeSelf == true && wasSet == false)
            {
                // We play the animation that focuses on the healer 
                wasSet = true;
                camAnim.Play("Healer");
            }

            //If it's the melee's turn and the animation wasn't set yet 
            if (meele.activeSelf == true && wasSet == false)
            {
                // We play the animation that focuses on the melee
                wasSet = true;
                camAnim.Play("Meele");
            }

            //If it's the range's turn and the animation wasn't set yet 
            if (range.activeSelf == true && wasSet == false)
            {
                // We play the animation that focuses on the range
                wasSet = true;
                camAnim.Play("Range");
            }

            //If it's the tank's turn and the animation wasn't set yet 
            if (tank.activeSelf == true && wasSet == false)
            {
                // We play the animation that focuses on the tank 
                wasSet = true;
                camAnim.Play("Tank");
            }

            //If it's the dragon's turn and the animation wasn't set yet 
            if (dragon.activeSelf == true && wasSet == false)
            {
                // We play the animation that focuses on the dragon
                wasSet = true;
                camAnim.Play("Dragon");
            }
        }
    }
}
